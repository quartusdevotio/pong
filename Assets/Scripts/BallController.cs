using UnityEngine;

namespace Pong.Ball
{
    public class BallController : MonoBehaviour
    {
        public Vector2 direction;
        [SerializeField] private Rigidbody2D rigidBody2D;
        [SerializeField] private float initialMovementSpeed = 5f;
        [SerializeField] private AudioSource hitSound;
        private bool isActive = true;

        void FixedUpdate()
        {
            if (!isActive)
            {
                return;
            }

            rigidBody2D.MovePosition(rigidBody2D.position + direction * Time.fixedDeltaTime);
        }

        public void Reset()
        {
            transform.position = Vector3.zero;
            direction = Vector2.zero;
        }

        public void Activate()
        {
            isActive = true;
        }

        public void Deactivate()
        {
            isActive = false;
        }

        public void RandomizeDirection(int side)
        {
            int directionX;
            if(side == 0)
            {
                directionX = -1;
            }
            else
            {
                directionX = 1;
            }
            direction = new Vector2(directionX, Random.Range(0f, 1f)).normalized * initialMovementSpeed;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Vector2 normalX = new Vector2(-direction.x, 0f).normalized;
            Vector2 normalY = new Vector2(0f, -direction.y).normalized;
            if (collision.CompareTag("Border"))
            {
                direction = direction - 2 * (Vector2.Dot(direction, normalY) * normalY);
            }
            else if(collision.CompareTag("Player"))
            {
                direction = direction - 2 * (Vector2.Dot(direction, normalX) * normalX);
                hitSound.Play();
            }
        }
    }
}