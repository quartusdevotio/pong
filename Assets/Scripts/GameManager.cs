using Pong.Ball;
using Pong.Goal;
using Pong.Player;
using Pong.UI;
using System;
using UnityEngine;

namespace Pong
{
    public enum GameState
    {
        None,
        Starting,
        Playing,
        Paused,
    };

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private BallController ball;
        [SerializeField] private PlayerController playerA;
        [SerializeField] private PlayerController playerB;
        [SerializeField] private GoalController goalA;
        [SerializeField] private GoalController goalB;
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;

        [SerializeField] private GameState gameState = GameState.None;

        public event Action<int, int> ScoreChanged;
        public event Action FirstBallEjected;
        [SerializeField] private int winScore = 10;
        private int lastScored;
        private int scoreA = 0;
        private int scoreB = 0;
        private bool isPaused = false;

        public event Action Restarted;

        [SerializeField] private AudioSource pauseSound;
        [SerializeField] private AudioSource gameOverSound;

        // Start is called before the first frame update
        void Start()
        {
            Initialize();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && gameState == GameState.Starting && !isPaused)
            {
                StartGame();
                gameState = GameState.Playing;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Pause();
            }
        }

        private void Initialize()
        {
            goalA.GoalScored += OnGoalScored;
            goalB.GoalScored += OnGoalScored;
            pausePopup.ContinuePressed += OnContinuePressed;
            pausePopup.RestartPressed += OnRestartPressed;
            pausePopup.QuitPressed += OnQuitPressed;
            pausePopup.PlayerASwitched += OnPlayerASwitched;
            pausePopup.PlayerBSwitched += OnPlayerBSwitched;

            gameOverPopup.PlayAgainPressed += OnRestartPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;

            gameState = GameState.Starting;

            lastScored = UnityEngine.Random.Range(0, 2);
        }

        private void OnDestroy()
        {
            goalA.GoalScored -= OnGoalScored;
            goalB.GoalScored -= OnGoalScored;
            pausePopup.ContinuePressed -= OnContinuePressed;
            pausePopup.RestartPressed -= OnRestartPressed;
            pausePopup.QuitPressed -= OnQuitPressed;
            pausePopup.PlayerASwitched -= OnPlayerASwitched;
            pausePopup.PlayerBSwitched -= OnPlayerBSwitched;

            gameOverPopup.PlayAgainPressed -= OnRestartPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
        }

        private void StartGame()
        {
            EjectBall();
            FirstBallEjected?.Invoke();
        }

        private void EjectBall()
        {
            ball.RandomizeDirection(lastScored);
            if(!isPaused)
            {
                ball.Activate();
            }
        }

        private void OnGoalScored(int side)
        {
            if(side == 0)
            {
                scoreB++;
                lastScored = 1;
            }
            else
            {
                scoreA++;
                lastScored = 0;
            }
            ScoreChanged?.Invoke(scoreA, scoreB);

            if(GameOverCheck())
            {
                return;
            }

            ball.Reset();
            Invoke("EjectBall", 2f);
        }

        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnRestartPressed()
        {
            Restart();
        }

        private void OnQuitPressed()
        {
            Quit();
        }

        private void OnPlayerASwitched()
        {
            playerA.SwitchPlayer();
        }

        private void OnPlayerBSwitched()
        {
            playerB.SwitchPlayer();
        }

        private void Pause()
        {
            if(isPaused)
            {
                playerA.Activate();
                playerB.Activate();
                ball.Activate();
                isPaused = false;
                pausePopup.Hide();
            }
            else
            {
                playerA.Deactivate();
                playerB.Deactivate();
                ball.Deactivate();
                isPaused = true;
                pausePopup.Show();
                pauseSound.Play();
            }

        }

        private void Restart()
        {
            if(isPaused)
            {
                Pause();
            }
            else
            {
                playerA.Activate();
                playerB.Activate();
            }
            Restarted?.Invoke();
            ball.Reset();
            ball.Deactivate();
            playerA.transform.position = new Vector2(-7.5f, 0f);
            playerB.transform.position = new Vector2(7.5f, 0f);
            gameState = GameState.Starting;
            ResetScore();
        }

        private void ResetScore()
        {
            scoreA = 0;
            scoreB = 0;
            ScoreChanged?.Invoke(scoreA, scoreB);
        }

        private void Quit()
        {
            Application.Quit();
        }

        private bool GameOverCheck()
        {
            bool winnerShowUp = false;
            if(scoreA == winScore)
            {
                gameOverPopup.SetPlayerWin(0);
                winnerShowUp = true;
            }
            else if(scoreB == winScore)
            {
                gameOverPopup.SetPlayerWin(1);
                winnerShowUp = true;
            }

            if(winnerShowUp)
            {
                playerA.Deactivate();
                playerB.Deactivate();
                ball.Deactivate();
                gameOverPopup.Show();
                gameOverSound.Play();
                return true;
            }
            return false;
        }
    }
}