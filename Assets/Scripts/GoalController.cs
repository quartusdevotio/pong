using System;
using UnityEngine;

namespace Pong.Goal
{
    public class GoalController : MonoBehaviour
    {
        public event Action<int> GoalScored;
        private int side;
        [SerializeField] private AudioSource goalSound;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.CompareTag("Ball"))
            {
                float dotResult = transform.position.x - collision.transform.position.x;
                if(dotResult < 0)
                {
                    side = 0;
                }
                else
                {
                    side = 1;
                }
                GoalScored?.Invoke(side);
                goalSound.Play();
            }
        }
    }

}