using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pong.UI
{
    public class PausePopupController : PopupController
    {
        public Action ContinuePressed;
        public Action RestartPressed;
        public Action QuitPressed;
        public Action PlayerASwitched;
        public Action PlayerBSwitched;

        [SerializeField] private Button continueButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private Button playerASwitchButton;
        [SerializeField] private Button playerBSwitchButton;

        private void Start()
        {
            continueButton.onClick.AddListener(OnContinuePressed);
            restartButton.onClick.AddListener(OnRestartPressed);
            quitButton.onClick.AddListener(OnQuitPressed);
            playerASwitchButton.onClick.AddListener(OnPlayerASwitched);
            playerBSwitchButton.onClick.AddListener(OnPlayerBSwitched);
        }

        private void OnDestroy()
        {
            continueButton.onClick.RemoveListener(OnContinuePressed);
            restartButton.onClick.RemoveListener(OnRestartPressed);
            quitButton.onClick.RemoveListener(OnQuitPressed);
            playerASwitchButton.onClick.RemoveListener(OnPlayerASwitched);
            playerBSwitchButton.onClick.RemoveListener(OnPlayerBSwitched);
        }

        private void OnContinuePressed()
        {
            Hide();
            ContinuePressed?.Invoke();
        }

        private void OnRestartPressed()
        {
            Hide();
            RestartPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            QuitPressed?.Invoke();
        }

        public void OnPlayerASwitched()
        {
            PlayerASwitched?.Invoke();
        }

        public void OnPlayerBSwitched()
        {
            PlayerBSwitched?.Invoke();
        }
    }

}