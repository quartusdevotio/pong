using Pong.Ball;
using UnityEngine;

namespace Pong.Player
{
    public class PlayerController : MonoBehaviour
    {

        [SerializeField] public Rigidbody2D rigidBody2D;
        [SerializeField] private KeyCode upInput;
        [SerializeField] private KeyCode downInput;
        [SerializeField] private float topBoundary;
        [SerializeField] private float bottomBoundary;
        [SerializeField] private float movementSpeed;

        private bool isActive = true;
        [SerializeField] private bool aiControlled = false;
        [SerializeField] private BallController ballController;
        [SerializeField] private Material playerMaterial;
        [SerializeField] private Material aiMaterial;
        [SerializeField] private MeshRenderer meshRenderer;

        void Update()
        {
            if(!isActive)
            {
                return;
            }

            if(aiControlled)
            {
                if(transform.position.y < ballController.transform.position.y && transform.position.y < topBoundary)
                {
                    transform.Translate(new Vector3(0f, movementSpeed) * Time.deltaTime);
                }
                else if (transform.position.y > ballController.transform.position.y && transform.position.y > bottomBoundary)
                {
                    transform.Translate(new Vector3(0f, -movementSpeed) * Time.deltaTime);
                }
                return;
            }

            if(Input.GetKey(upInput) && transform.position.y < topBoundary)
            {
                transform.Translate(new Vector3(0f, movementSpeed) * Time.deltaTime);
            }
            else if (Input.GetKey(downInput) && transform.position.y > bottomBoundary)
            {
                transform.Translate(new Vector3(0f, -movementSpeed) * Time.deltaTime);
            }
        }

        public void SwitchPlayer()
        {
            aiControlled = !aiControlled;
            if(aiControlled)
            {
                meshRenderer.material = aiMaterial;
            }
            else
            {
                meshRenderer.material = playerMaterial;
            }
        }

        public void Activate()
        {
            isActive = true;
        }

        public void Deactivate()
        {
            isActive = false;
        }
    }
}