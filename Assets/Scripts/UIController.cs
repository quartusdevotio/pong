using TMPro;
using UnityEngine;

namespace Pong.UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TMP_Text scoreAText;
        [SerializeField] private TMP_Text scoreBText;
        [SerializeField] private TMP_Text startText;

        private void Start()
        {
            gameManager.ScoreChanged += OnScoreChanged;
            gameManager.FirstBallEjected += OnFirstBallEjected;
            gameManager.Restarted += OnFirstBallEjected;
        }

        private void OnDestroy()
        {
            gameManager.ScoreChanged -= OnScoreChanged;
            gameManager.FirstBallEjected -= OnFirstBallEjected;
        }

        private void OnScoreChanged(int scoreA, int scoreB)
        {
            scoreAText.text = scoreA.ToString();
            scoreBText.text = scoreB.ToString();
        }

        public void OnFirstBallEjected()
        {
            startText.enabled = false;
        }
    }
}